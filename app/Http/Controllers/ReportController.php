<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Product;
use App\Gudang;

class ReportController extends Controller
{
    //
    public function index()
    {
      $products = Product::all();
      $gudangs= Gudang::all();
      $page = DB::table('products')->paginate(10);
      return view('report.index', compact('page','products','gudangs'));
    }





    // public function getId($id)
    // {
    //     $products= Product::find($id);
    // }
    public function store(Request $request)
    {
      // dd($request->all());
      $products = DB::table('products')->where('id', $request->products)->first();
      $amount = $request->input('amount');
      $inout = $request->input('inout');
      // dd($request->all());
      if($request->inout==1)
      {
              if ($request->gudangs==1) {
                $newQty = $products->bl_stock + $amount;
                  // DB::enableQueryLog();
                DB::table('products')->where('id', $request->products)->update(['bl_stock' => $newQty]);

              } else if($request->gudangs==2){
                $newQty = $products->tokpemovi_stock + $amount;
                DB::table('products')->where('id', $request->products)->update(['tokpemovi_stock' => $newQty]);
              }
                else if($request->gudangs==3){
                $newQty = $products->tokepdsel_stock + $amount;
                DB::table('products')->where('id', $request->products)->update(['tokepdsel_stock' => $newQty]);
              }
                else if($request->gudangs==4){
                $newQty = $products->shopee_stock + $amount;
                DB::table('products')->where('id', $request->products)->update(['shopee_stock' => $newQty]);
              }
              else {
                  $newQty = $products->waline_stock + $amount;
                  DB::table('products')->where('id', $request->products)->update(['waline_stock' => $newQty]);
                }
            // return redirect()->route('report.index');
        }
        else
              if ($request->gudangs==1) {
                  $newQty = $products->bl_stock - $amount;
                  DB::table('products')->where('id', $request->products)->update(['bl_stock' => $newQty]);
              } else if($request->gudangs==2){
                $newQty = $products->tokpemovi_stock - $amount;
                DB::table('products')->where('id', $request->products)->update(['tokpemovi_stock' => $newQty]);
              }
                else if($request->gudangs==3){
                $newQty = $products->tokepdsel_stock - $amount;
                DB::table('products')->where('id', $request->products)->update(['tokepdsel_stock' => $newQty]);
              }
                else if($request->gudangs==4){
                $newQty = $products->shopee_stock - $amount;
                DB::table('products')->where('id', $request->products)->update(['shopee_stock' => $newQty]);
              }
              else {
                  $newQty = $products->waline_stock - $amount;
                  DB::table('products')->where('id', $request->products)->update(['waline_stock' => $newQty]);
                }
          return redirect()->route('report.index');
      }




}
