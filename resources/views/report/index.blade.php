@extends('layouts.app')
@section('content')
<!-- Update Jurnal -->
<div class="container">
<div class="card">
    <div class="card-body">
        <h4 class="card-title">Update Jurnal</h4>
      <form class="" action="{{ route('report.store') }}" method="post">
          {{ csrf_field () }}
          {{ method_field('PATCH')}}
          <!-- <div class="form-group">
            <label for="">Title</label>
            <input type="text" class="form-control" name="title" placeholder="Post Title">
          </div> -->
          <div class="form-group">
            <label for="">Product Name</label>
            <select class="form-control" name="products">
                <option value="0" disable="true" selected="true">=== Select Product ===</option>
            @foreach ($products as $key => $p)
              <option value="{{$p->id}}">{{ $p->name }}</option>
            @endforeach
            </select>
          </div>
          <div class="form-group">
            <label for="">Resource Name</label>
            <select class="form-control" name="gudangs">
                <option value="0" disable="true" selected="true">=== Select Gudang ===</option>
            @foreach ($gudangs as $key => $g)
              <option value="{{$g->id}}">{{ $g->name }}</option>
            @endforeach
            </select>
          </div>
          <div class="form-group">
            <label for="">In / Out </label>
            <select class="form-control" name="inout">
                <option value="0" disable="true" selected="true">=== In / Out ===</option>
                <option value="1">In</option>
                <option value="2">Out</option>
            </select>
          </div>
          <div class="form-group">
            <label for="">Amount</label>
            <input type="text" class="form-control" name="amount" placeholder="Input Amount">
          </div>
          <!-- <div class="form-group">
            <label for="">Content</label>
            <textarea name="content" rows="5" class="form-control" cols="80" placeholder="Content"></textarea>
          </div> -->
          <div class="form-group">
            <input type="submit" name="submit" class="btn btn-primary" value="Submit">
          </div>


      </form>
    </div>
  </div>
</div>
<br></br>
<!-- Table Jurnal -->
<div class="container">
    <div class="row justify-content-center" >
            <table class="table table-stripped">
                <thead class="thead-primary">
                    <tr>
                        <th class="text-center">Product ID</th>
                        <th>Nama Product</th>
                        <th>Stock Tokopedia MOVI</th>
                        <th>Stock Tokopedia Selatan</th>
                        <th>Stock Bukalapak</th>
                        <th>Stock Shopee</th>
                        <th>Stock WA & Line </th>
                        <th>Total Stock</th>
                        <th>Update Terakhir</th>
                        <!-- <th>Action</th> -->
                    </tr>
                </thead>

                <?php $i = 1; ?>
        @foreach($page as $p)
        <tr>
            <!-- <td class="text-center">{{$loop->iteration}}</td> -->
            <td>{{$p->id}}</td>
            <td>{{$p->name}}</td>
            <td>{{$p->tokpemovi_stock}}</td>
            <td>{{$p->tokepdsel_stock}}</td>
            <td>{{$p->bl_stock}}</td>
            <td>{{$p->shopee_stock}}</td>
            <td>{{$p->waline_stock}}</td>
            <td>{{ $p->tokpemovi_stock + $p->tokepdsel_stock + $p->bl_stock + $p->shopee_stock + $p->waline_stock }}
            <td class="text-center">{{$p->updated_at}}</td>


        </tr>

        @endforeach
        </tbody>
            </table>
        </div>
        <nav aria-label="Page navigation">
        <ul class="pagination">
          <li>{{ $page->links() }}</li>
          <li></li>
          <li class="page-item"><a class="page-link">{{ $page->count() }} Data</a></li>
          <li class="page-item"><a class="page-link">From</a></li>
          <li class="page-item"><a class="page-link">{{ $page->total() }} Data</a></li>
        </ul>
      </nav>
    </div>


@endsection
